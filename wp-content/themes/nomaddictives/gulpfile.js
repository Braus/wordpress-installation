var gulp        = require('gulp'),
    jshint      = require('gulp-jshint'),
    browserSync = require('browser-sync'),
    sass        = require('gulp-sass'),
    path        = require('path'),
    clean       = require('gulp-clean'),
    concat      = require('gulp-concat'),
    autoprefixer = require('gulp-autoprefixer'),
    cleanCSS    = require('gulp-clean-css'),
    rename      = require('gulp-rename'),
    filesize    = require('gulp-filesize'),
    notify      = require('gulp-notify'),
    plumber     = require('gulp-plumber'),
    order       = require('gulp-order'),
    sourcemaps  = require('gulp-sourcemaps');

var mainBowerFiles  = require('main-bower-files'),
    filter          = require('gulp-filter'),
    uglify          = require('gulp-uglify');

var cache           = require('gulp-cache'),
    imagemin        = require('imagemin');

var config = require('./gulp-config.json');


// Compile and minify all sass files under the sass folder and send them to css folder.
// main.min.css
gulp.task('sass', function() {

    var sassFiles = [
        './assets/sass/**/*.scss'
    ];

    return gulp.src(sassFiles)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('main.css'))
        .pipe(sass({
            paths:[path.join(__dirname, 'sass')]
        }).on('error', sass.logError))
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false
        }))
        .pipe(cleanCSS({debug: true}, function(details) {
            console.log(details.name + ': ' + details.stats.originalSize);
            console.log(details.name + ': ' + details.stats.minifiedSize);
        }))
        .pipe(rename({suffix: '.min'}))
        .pipe(filesize())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('./dist/css'))
        .pipe(browserSync.stream())
        .pipe(notify({ message: "Style compressed", onLast: true}) );
});

// Compile all js in javascript folder and send it to js folder.
// main.min.js
gulp.task('js', function() {

    var jsFiles = [
        './assets/js/**/*.js'
    ];

    gulp.src(jsFiles)
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(concat('main.js'))
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(filesize())
        .pipe(sourcemaps.write('maps'))
        .pipe(gulp.dest('./dist/js'))
        .pipe(browserSync.stream())
        .pipe(notify({ message: "Javascript is uglyfied", onLast: true}) );

});

// Compile all js registered in bower.json file and send to js folder too.
// vendor.min.js
gulp.task('vendor_js', function(){
    console.log(mainBowerFiles({bowerJson: 'bower.json'}));
    gulp.src( mainBowerFiles({bowerJson: 'bower.json'}) )
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(filter('**/*.js', {restore: true}))
        .pipe(concat('vendor.js'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(filesize())
        .pipe(sourcemaps.write('maps'))
        .pipe(plumber.stop())
        .pipe(gulp.dest('./dist/js'))
        .pipe(notify({message: "Vendor js is successfully compressed", onLast: true}));
});

// Compress fonts and send to fonts folder.
gulp.task('fonts', function(){

    var fonts = [
        './assets/fonts/**/*'
    ]

    return gulp.src(fonts)
        .pipe(gulp.dest('./dist/fonts'))
        .pipe(notify({message: "Fonts are compressed", onLast: true}));

});

// Watch all tasks and generate synced browser with the updated changes.
gulp.task('watch', [], function () {
    browserSync.init({
        proxy: config.proxy,
        open: true,
        ghostMode: true,
        logPrefix: config.logPrefix,
        snippetOptions: {
            whitelist: [],
            blacklist: []
        }
    });
    gulp.watch('./assets/sass/**/*', ['sass']);
    gulp.watch('./assets/js/**/*', ['js']);
    gulp.watch(['./**/*.php', './**/*.twig'], browserSync.reload);
    gulp.watch(['./assets/fonts/**/*'], ['fonts']);
});

// Type gulp 'dev or vendor' or just gulp in the terminal to run the tasks
gulp.task('dev', ['js', 'sass', 'watch']);
gulp.task('vendor', ['vendor_js']);
gulp.task('default', ['vendor_js', 'js', 'sass', 'fonts']);