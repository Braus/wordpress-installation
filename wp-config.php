<?php
/**
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

if (!defined('WP_SITEURL')) {
    define('WP_SITEURL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp');
}
if (!defined('WP_HOME')) {
    define('WP_HOME', 'http://' . $_SERVER['HTTP_HOST']);
}

/* use wp-content from root directory  */
if (!defined('WP_CONTENT_DIR')) {
    define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content' );
}
if (!defined('WP_CONTENT_URL')) {
    define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp-content' );
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if ( file_exists( dirname( __FILE__ ) . '/wp-config-local.php' ) ) {

    define( 'WP_LOCAL_DEV', true );
    define( 'WP_STAGING_DEV', false );
    include( dirname( __FILE__ ) . '/wp-config-local.php' );

} elseif (file_exists( dirname( __FILE__ ) . '/wp-config-staging.php' ) ) {

    define( 'WP_LOCAL_DEV', false );
    define( 'WP_STAGING_DEV', true );
    include( dirname( __FILE__ ) . '/wp-config-staging.php' );

} else {
    define( 'WP_LOCAL_DEV', false );
    define( 'WP_STAGING_DEV', false );

    // ** MySQL settings - You can get this info from your web host ** //
    /** The name of the database for WordPress */
    define('DB_NAME', 'database_name_here');

    /** MySQL database username */
    define('DB_USER', 'username_here');

    /** MySQL database password */
    define('DB_PASSWORD', 'password_here');

    /** MySQL hostname */
    define('DB_HOST', 'localhost');

    /** Database Charset to use in creating database tables. */
    define('DB_CHARSET', 'utf8');

    /** The Database Collate type. Don't change this if in doubt. */
    define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J09%!jm[<<~-+{lWoF+S-Avy^Sg(,Zr1`zhpcJ_d@3i%wwaSUN+]KJ{U1U~y{$5+');
define('SECURE_AUTH_KEY',  'nX0|Z<u9ZX9|mQ6-,ecdNFkhJ|~G8TQ52Wxu@rn/Ys^!JUp}%n4J&K|+XMGh5Vb&');
define('LOGGED_IN_KEY',    '|r|m&SSkeMApWd(>(|xlaZiDAaPt1n+C4$e]vMmtt7Qr=~M<Y9`}hW4b:Rbb`Qf2');
define('NONCE_KEY',        '+]rnf>/-)O4G(QJ<Bx{^Q%U);}z~++W+#A[ V:sLmBDWGznO<+6MjOc9[~@u|u2n');
define('AUTH_SALT',        'JG1d]je,`b+|~Q|fc3b;KMt;^8&n!37e.t?)|E58w#|^fs6S/1EdWCwU-|CK;k^M');
define('SECURE_AUTH_SALT', '!X6PuMg{3_Kg,wMbxAl#jQpeB@i6L-#%az#-!H69+ot;D,Mw=mb50r@)Al5FJCz9');
define('LOGGED_IN_SALT',   'bX1G<-}mgf@<hl(2}ld YK4*Vb(6uFdR+Vb`i8Oky>pqbz`:;5Chuc>d_+Gj`B{r');
define('NONCE_SALT',       '9sBLt=Z,+visQ9- D@O-jc@q)~pvsp>fRBvs -QI&1K]6n6^U9H&+FA[H+xdH<}V');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
