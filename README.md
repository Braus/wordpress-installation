# Wordpress installation

`git clone this branch`<br/>
`cd /path/to/project`<br/>
`composer install`


## Branch

`master` has `gulp` and `bower`. This is an old method that I used before. 

check `webpack` branch for newer method.
 
## Theme

 Please update style.css
 
## Gulp & Bower
 
Go to the new theme folder and run `bower install`, `composer install` and `gulp dev` for development and `gulp` for production
 
## Webpack

Go to the new theme folder and run `yarn run dev` for development, `yarn run build` for production and `yarn run clean`